/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.5.5-10.1.16-MariaDB : Database - gc_demo_theme
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`gc_demo_theme` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `gc_demo_theme`;

/*Table structure for table `ci_sessions` */

DROP TABLE IF EXISTS `ci_sessions`;

CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `ip_address` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `user_agent` varchar(150) COLLATE utf8_bin NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `ci_sessions` */

/*Table structure for table `employees` */

DROP TABLE IF EXISTS `employees`;

CREATE TABLE `employees` (
  `employeeNumber` int(11) NOT NULL AUTO_INCREMENT,
  `lastName` varchar(50) NOT NULL,
  `firstName` varchar(50) NOT NULL,
  `extension` varchar(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `officeCode` varchar(10) NOT NULL,
  `file_url` varchar(250) CHARACTER SET utf8 NOT NULL,
  `jobTitle` varchar(50) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`employeeNumber`)
) ENGINE=MyISAM AUTO_INCREMENT=1705 DEFAULT CHARSET=latin1;

/*Data for the table `employees` */

insert  into `employees`(`employeeNumber`,`lastName`,`firstName`,`extension`,`email`,`officeCode`,`file_url`,`jobTitle`,`description`) values (1002,'Murphy','Diane','x5800','dmurphy@classicmodelcars.com','1','Koala.jpg','President',''),(1102,'Bondur','Gerard','x5408','gbondur@classicmodelcars.com','4','Chrysanthemum.jpg','Sale Manager (EMEA)',''),(1188,'Firrelli','Julie','x2173','jfirrelli@classicmodelcars.com','2','Desert.jpg','Sales Rep',''),(1703,'Test','Testing','512462','test@gmail.com','145','Hydrangeas.jpg','Prime Minister','<figure class=\"easyimage easyimage-full\"><img alt=\"\" src=\"blob:http://192.168.2.30/79873d64-afb9-4376-b054-3ccfdf6fdcb2\" width=\"1366\" />\r\n<figcaption>TEST</figcaption>\r\n</figure>\r\n\r\n<p>Hii thsi is editor testing with image.</p>\r\n'),(1704,'Test','Testing','512462','govorg4@gmail.com','145','Tulips-5ffbb.jpg','Prime Minister','<p>test</p>\r\n');

/*Table structure for table `login_attempts` */

DROP TABLE IF EXISTS `login_attempts`;

CREATE TABLE `login_attempts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(40) COLLATE utf8_bin NOT NULL,
  `login` varchar(50) COLLATE utf8_bin NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `login_attempts` */

/*Table structure for table `offices` */

DROP TABLE IF EXISTS `offices`;

CREATE TABLE `offices` (
  `officeCode` int(10) NOT NULL AUTO_INCREMENT,
  `city` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `addressLine1` varchar(50) NOT NULL,
  `addressLine2` varchar(50) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `country` varchar(50) NOT NULL,
  `postalCode` varchar(15) NOT NULL,
  `territory` varchar(10) NOT NULL,
  PRIMARY KEY (`officeCode`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `offices` */

insert  into `offices`(`officeCode`,`city`,`phone`,`addressLine1`,`addressLine2`,`state`,`country`,`postalCode`,`territory`) values (1,'San Francisco','+1 650 219 4782','100 Market Street','Suite 300','CA','USA','94080','NA'),(2,'Boston','+1 215 837 0825','1550 Court Place','Suite 102','MA','USA','02107','NA'),(3,'NYC','+1 212 555 3000','523 East 53rd Street','apt. 5A','NY','USA','10022','NA'),(4,'Paris','+33 14 723 4404','43 Rue Jouffroy D','','','France','75017','EMEA'),(5,'Tokyo','+81 33 224 5000','4-1 Kioicho',NULL,'Chiyoda-Ku','Japan','102-8578','Japan'),(6,'Sydney','+61 2 9264 2451','5-11 Wentworth Avenue','Floor #2',NULL,'Australia','NSW 2010','APAC'),(7,'London','+44 20 7877 2041','25 Old Broad Street','Level 7',NULL,'UK','EC2N 1HN','EMEA');

/*Table structure for table `user_autologin` */

DROP TABLE IF EXISTS `user_autologin`;

CREATE TABLE `user_autologin` (
  `key_id` char(32) COLLATE utf8_bin NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `user_agent` varchar(150) COLLATE utf8_bin NOT NULL,
  `last_ip` varchar(40) COLLATE utf8_bin NOT NULL,
  `last_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`key_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `user_autologin` */

/*Table structure for table `user_profiles` */

DROP TABLE IF EXISTS `user_profiles`;

CREATE TABLE `user_profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `country_id` varchar(3) COLLATE utf8_bin DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `state_id` smallint(5) unsigned DEFAULT NULL,
  `city_id` mediumint(8) unsigned DEFAULT NULL,
  `user_address` text COLLATE utf8_bin,
  `user_pincode` varchar(6) COLLATE utf8_bin DEFAULT NULL,
  `b_step_1` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-blank 1-entered',
  `file_id` bigint(20) DEFAULT NULL,
  `is_google_map` tinyint(2) DEFAULT NULL,
  `latitude` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `longitude` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `place_id` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  `formatted_address` text COLLATE utf8_bin,
  `session_id` int(5) DEFAULT NULL,
  `session_year` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=37388 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `user_profiles` */

insert  into `user_profiles`(`id`,`user_id`,`country_id`,`website`,`state_id`,`city_id`,`user_address`,`user_pincode`,`b_step_1`,`file_id`,`is_google_map`,`latitude`,`longitude`,`place_id`,`formatted_address`,`session_id`,`session_year`) values (37387,43465,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE utf8_bin NOT NULL,
  `password` varchar(255) COLLATE utf8_bin DEFAULT '$P$B1wbyWRWtRs967/pJRam6CC6upLbIf/' COMMENT 'Back Up Of Old Ones',
  `email` varchar(100) COLLATE utf8_bin NOT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT '1',
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `ban_reason` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `new_password_key` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `new_password_requested` datetime DEFAULT NULL,
  `new_email` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `new_email_key` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `last_ip` varchar(40) COLLATE utf8_bin NOT NULL,
  `last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `allsport_id` varchar(200) COLLATE utf8_bin NOT NULL,
  `allsport_tmp_id` bigint(20) NOT NULL,
  `user_level` int(4) DEFAULT NULL,
  `main_category` int(4) DEFAULT NULL,
  `category_type` int(4) DEFAULT NULL,
  `sub_category_type` int(5) DEFAULT NULL,
  `admin_username` varchar(500) COLLATE utf8_bin DEFAULT NULL,
  `activate_profile` tinyint(4) DEFAULT NULL COMMENT '0-not active 1-active',
  `tmp_usermod_act_time` datetime DEFAULT NULL,
  `tmp_user_expire` datetime DEFAULT NULL,
  `password_test` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `vp_act` datetime DEFAULT NULL,
  `userid_type` enum('n','v') COLLATE utf8_bin DEFAULT 'n',
  `password_cp` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `password_bckp` varchar(255) COLLATE utf8_bin NOT NULL,
  `email_bckp` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`,`allsport_id`,`allsport_tmp_id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `allsport_tmp_id` (`allsport_tmp_id`),
  KEY `user_level` (`user_level`),
  KEY `main_category` (`main_category`),
  KEY `category_type` (`category_type`),
  KEY `sub_category_type` (`sub_category_type`)
) ENGINE=InnoDB AUTO_INCREMENT=43466 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `users` */

insert  into `users`(`id`,`username`,`password`,`email`,`activated`,`banned`,`ban_reason`,`new_password_key`,`new_password_requested`,`new_email`,`new_email_key`,`last_ip`,`last_login`,`created`,`modified`,`allsport_id`,`allsport_tmp_id`,`user_level`,`main_category`,`category_type`,`sub_category_type`,`admin_username`,`activate_profile`,`tmp_usermod_act_time`,`tmp_user_expire`,`password_test`,`vp_act`,`userid_type`,`password_cp`,`password_bckp`,`email_bckp`) values (43465,'','$2a$08$keJDCPxB5phhz24l2xHHO./4.btF..DEY6HkqE.SyIsdNv50rFDti','admin@gmail.com',1,0,NULL,NULL,NULL,NULL,NULL,'192.168.2.30','2020-02-27 12:39:34','2020-02-27 05:48:59','2020-02-27 17:09:34','',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'n',NULL,'',NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
