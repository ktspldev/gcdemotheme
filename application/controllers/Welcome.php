<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->library('form_validation');        
        $this->load->helper(array('form', 'url'));
        $this->load->helper('file');
        $this->load->helper('security');
        $this->load->library('tank_auth');
        $this->lang->load('tank_auth');
        $this->load->library('grocery_CRUD');
    }

    function _example_output($output = null) 
    {
        $data['output'] = (array)$output;
        $data['load_grocery_crud'] = 'yes';
        
        if (isset($output->isJSONResponse) && $output->isJSONResponse)
        {
            header('Content-Type: application/json; charset=utf-8');
            echo $output->output;
            exit;
        }
        $this->load->view('parts/header', $data);
        $this->load->view('parts/right_sidebar', $data);
        $this->load->view('parts/left_sidebar', $data);
        $this->load->view('parts/right_sidebar_one', $data);
        $this->load->view('index', $data);
        $this->load->view('parts/footer', $data);
    }

    function index()
    {
        if (!$this->tank_auth->is_logged_in())
        {
            redirect('/auth/login/');
        } 
        else
        {
            $crud = new grocery_CRUD();
            $crud->unset_bootstrap();
            $crud->unset_jquery();
			$crud->set_theme('tablestrap');
			$crud->set_table('employees');
			$crud->set_relation('officeCode','offices','city');
			$crud->display_as('officeCode','Office City');
			$crud->set_subject('Employee');

			$crud->required_fields('lastName');

			$crud->set_field_upload('file_url','assets/uploads/files');

			$output = $crud->render();

			$this->_example_output($output);
        }
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */