﻿<?php
    $login = array(
        'name'  => 'login',
        'id'    => 'login',
        'value' => set_value('login'),
        'maxlength' => 80,
        'size'  => 30,
    );
    if ($login_by_username AND $login_by_email) {
        $login_label = 'Email or login';
    } else if ($login_by_username) {
        $login_label = 'Login';
    } else {
        $login_label = 'Email';
    }
    $password = array(
        'name'  => 'password',
        'id'    => 'password',
        'size'  => 30,
    );
    $remember = array(
        'name'  => 'remember',
        'id'    => 'remember',
        'value' => 1,
        'checked'   => set_value('remember'),
        'style' => 'margin:0;padding:0',
    );
    $captcha = array(
        'name'  => 'captcha',
        'id'    => 'captcha',
        'maxlength' => 8,
    );
?>

<div class="authentication">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-sm-12">
                <?php echo form_open($this->uri->uri_string(), array("class" => "card auth_form")); ?>
                    <div class="header">
                        <img class="logo" src="<?php echo base_url(); ?>assets/images/logo.svg" alt="">
                        <h5>Log in</h5>
                    </div>
                    <div class="body">
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" placeholder="Username" name="login">
                            <div class="input-group-append">
                                <span class="input-group-text"><i class="zmdi zmdi-account-circle"></i></span>
                            </div>

                        </div>
                        <p style="color: red;"><?php echo form_error($login['name']); ?><?php echo isset($errors[$login['name']])?$errors[$login['name']]:''; ?></p>
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" placeholder="Password" name="password">
                            <div class="input-group-append">
                                <span class="input-group-text"><a href="forgot-password.html" class="forgot" title="Forgot Password"><i class="zmdi zmdi-lock"></i></a></span>
                            </div>
                        </div>
                        <div class="checkbox">
                            <input id="remember_me" type="checkbox">
                            <label for="remember_me">Remember Me</label>
                        </div>
                        <input type="submit" name="Sign-in" class="btn btn-primary btn-block waves-effect waves-light" value="SIGN IN">
                    </div>
                </form>
                <div class="copyright text-center">

                </div>
            </div>
            <div class="col-lg-8 col-sm-12">
                <div class="card">
                    <img src="<?php echo base_url(); ?>assets/images/signin.svg" alt="Sign In"/>
                </div>
            </div>
        </div>
    </div>
</div>