﻿<?php
    $login = array(
        'name'  => 'login',
        'id'    => 'login',
        'value' => set_value('login'),
        'maxlength' => 80,
        'size'  => 30,
    );
    if ($login_by_username AND $login_by_email) {
        $login_label = 'Email or login';
    } else if ($login_by_username) {
        $login_label = 'Login';
    } else {
        $login_label = 'Email';
    }
    $password = array(
        'name'  => 'password',
        'id'    => 'password',
        'size'  => 30,
    );
    $remember = array(
        'name'  => 'remember',
        'id'    => 'remember',
        'value' => 1,
        'checked'   => set_value('remember'),
        'style' => 'margin:0;padding:0',
    );
    $captcha = array(
        'name'  => 'captcha',
        'id'    => 'captcha',
        'maxlength' => 8,
    );
?>
<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="">

<title>::GC Basic Demo :: Sign In</title>
<!-- Favicon-->
<link rel="icon" href="favicon.ico" type="image/x-icon">
<!-- Custom Css -->
<link rel="stylesheet" href="<?php echo base_url('assets/plugins/bootstrap/css/bootstrap.min.css');?>">
<link rel="stylesheet" href="<?php echo base_url('assets/css/style.min.css');?>">    
</head>

<body class="theme-blush">

<div class="authentication">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-sm-12">
                <?php echo form_open($this->uri->uri_string(), array("class" => "card auth_form")); ?>
                    <div class="header">
                        <img class="logo" src="<?php echo base_url('assets/images/logo.svg');?>" alt="">
                        <h5>Log in</h5>
                    </div>
                    <div class="body">
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" placeholder="Username"  name="login">
                            <div class="input-group-append">
                                <span class="input-group-text"><i class="zmdi zmdi-account-circle"></i></span>
                            </div>
                            <span style="color: red;">
                                <?php echo form_error($login['name']); ?>
                                <?php echo isset($errors[$login['name']])?$errors[$login['name']]:''; ?>
                            </span>
                        </div>
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" placeholder="Password" name="password">
                            <div class="input-group-append">                                
                                <span class="input-group-text"><i class="zmdi zmdi-lock"></i></span>
                            </div>                            
                        </div>
                        <div class="checkbox">
                            <input id="remember_me" type="checkbox">
                            <label for="remember_me">Remember Me</label>
                        </div>
                        <button type="submit" name="Sign-in" class="btn btn-primary btn-block waves-effect waves-light">Sign In</button>                       
                        <!-- <div class="signin_with mt-3">
                            <p class="mb-0">or Sign Up using</p>
                            <button class="btn btn-primary btn-icon btn-icon-mini btn-round facebook"><i class="zmdi zmdi-facebook"></i></button>
                            <button class="btn btn-primary btn-icon btn-icon-mini btn-round twitter"><i class="zmdi zmdi-twitter"></i></button>
                            <button class="btn btn-primary btn-icon btn-icon-mini btn-round google"><i class="zmdi zmdi-google-plus"></i></button>
                        </div> -->
                    </div>
                    <?php echo form_close(); ?>
                <div class="copyright text-center">
                    &copy;
                    <script>document.write(new Date().getFullYear())</script>,
                    <span>Designed by <a href="#" target="_blank">ThemeMakker</a></span>
                </div>
            </div>
            <div class="col-lg-8 col-sm-12">
                <div class="card">
                    <img src="<?php echo base_url('assets/images/signin.svg');?>" alt="Sign In"/>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Jquery Core Js -->
<script src="<?php echo base_url('assets/bundles/libscripts.bundle.js');?>"></script>
<script src="<?php echo base_url('assets/bundles/vendorscripts.bundle.js');?>"></script> <!-- Lib Scripts Plugin Js -->
</body>
</html>