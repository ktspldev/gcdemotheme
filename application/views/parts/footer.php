<!-- Jquery Core Js --> 
<script src="<?php echo base_url('assets/bundles/libscripts.bundle.js');?>"></script> <!-- Lib Scripts Plugin Js ( jquery.v3.2.1, Bootstrap4 js) --> 
<script src="<?php echo base_url('assets/bundles/vendorscripts.bundle.js');?>"></script> <!-- slimscroll, waves Scripts Plugin Js -->

<script src="<?php echo base_url('assets/bundles/jvectormap.bundle.js');?>"></script> <!-- JVectorMap Plugin Js -->
<script src="<?php echo base_url('assets/bundles/sparkline.bundle.js');?>"></script> <!-- Sparkline Plugin Js -->
<script src="<?php echo base_url('assets/bundles/c3.bundle.js');?>"></script>

<script src="<?php echo base_url('assets/bundles/mainscripts.bundle.js');?>"></script>
<script src="<?php echo base_url('assets/js/pages/index.js');?>"></script>

<?php
    if(isset($load_grocery_crud) && $load_grocery_crud == 'yes')
    {
        ?>
        <!-- Load Grocery Crud -->
        <?php 
        foreach($output['js_files'] as $file):?>
        <script src="<?php echo $file; ?>"></script>
        <?php endforeach;?>
        <!-- END Load Grocery Crud -->
        <?php
    }
?>
</body>
</html>